
from django.contrib import admin

# Register your models here.

from .models import Postagem  , VotoPostagemEleitor


class PostagemAdmin(admin.ModelAdmin):
    list_display  = ( 'id'  , 'autor' , "_votos" )
    
    def _votos( self , obj ):
        return VotoPostagemEleitor.objects.filter(postagem=obj.id).count()


admin.site.register(Postagem, PostagemAdmin)

class VotoPostagemEleitorAdmin(admin.ModelAdmin):
    list_display  = ( 'eleitor', 'postagem', "_votos" )

    def _votos(self , obj ):
        return VotoPostagemEleitor.objects.filter(postagem=obj.postagem ).count()
 
admin.site.register(VotoPostagemEleitor ,  VotoPostagemEleitorAdmin )
