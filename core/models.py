from django.db import models
#from django.dispatch import receiver
from stdimage.models import StdImageField
import os 

from django.contrib.auth.models import User
# Create your models here.

class Postagem( models.Model ):

    descricao = models.CharField("Descrição" , max_length=1000)
    imagem = StdImageField('Imagem' , upload_to="postagem",  variations={'thumb':(124,124)})
    autor = models.CharField( 'Autor' , max_length=255 ,null = True)

    class Meta:
        verbose_name = "Postagem"
        verbose_name_plural = "Postagens"


    def __str__(self):
        return self.descricao
    
    # @receiver(models.signals.post_delete, sender=Postagem)
    def auto_delete_file_on_delete(sender, instance, **kwargs):
    
        if instance.imagem:
            if os.path.isfile(instance.imagem.url):
                os.remove(instance.imagem.url)

    #@receiver(models.signals.pre_save, sender=Postagem)
    def auto_delete_file_on_change(sender, instance, **kwargs):

        if not instance.pk:
            return False

        try:
            old_file = Postagem.objects.get(id=instance.pk).imagem
        except Postagem.DoesNotExist:
            return False

        new_file = instance.imagem
        if not old_file == new_file:
            if os.path.isfile(old_file.path):
                os.remove(old_file.path)

class VotoPostagemEleitor( models.Model ):
    postagem = models.ForeignKey(Postagem , on_delete=models.CASCADE , null = True)
    eleitor = models.ForeignKey(User , on_delete = models.CASCADE, null = True ) 

    def __str__(self):
        return f'{self.eleitor} {self.postagem}'
    