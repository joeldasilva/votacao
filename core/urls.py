from django.urls import path
from django.contrib.auth.decorators import login_required

from .views import index , vote , vervotos, alterar, sobre

urlpatterns = [ 
    path( '', login_required(index), name='index'),
    path('vote/<int:id>/' , login_required(vote) , name="voto" ) ,
    path('seusvotos/' , login_required(vervotos) , name='seusvotos'),
    path('seusvotos/<int:id>' ,login_required(alterar), name='alterar'),
    path('sobre/<int:id>' , login_required(sobre) , name='sobre' )

]