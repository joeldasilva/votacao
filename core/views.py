from django.shortcuts import render

from django.shortcuts import render , redirect
from django.contrib import messages 
from .models import Postagem  , VotoPostagemEleitor 
from .forms import PostagemModelForm 

from django.contrib.auth.models import User


# Create your views here.

def index( request ):
    post =  Postagem.objects.all()
    data = list()
    itens = dict()
        
    for p in post:
        itens.clear()
        vt = VotoPostagemEleitor.objects.filter(postagem=p).count()
        e = VotoPostagemEleitor.objects.filter(postagem=p , eleitor = request.user.id)
        itens['id'] = p.id
        itens['desc'] = p.descricao 
        itens['imagem']= p.imagem.url 
        itens['autor'] = p.autor
        itens['eleitor'] = e.exists()
        itens['voto'] =  vt
        data.append( itens.copy())

         
        
    context = {
        'postagens' : data
    }

        
    return render(request , 'index.html' , context )

def vote( request , id ):
    
    cont = VotoPostagemEleitor.objects.filter(eleitor= request.user).count()

    if cont == 3:
        messages.error( request , 'voce ja votou 3 vezes')
    else:
        
        is_voted = VotoPostagemEleitor.objects.filter(postagem=id, eleitor = request.user)

        if not is_voted:
            post = Postagem.objects.get(id=id)
            VotoPostagemEleitor.objects.create(postagem = post, eleitor = request.user).save()
            messages.success(request,'Voto realizado com sucesso, você tem direito a no máximo 3 votos')
            messages.warning(request ,"É possível alterar seus votos clicando no mesmo ícone ou na aba 'Seus votos' ")
        else:
            messages.error(request, 'Voce ja votou nessa publicacao')
        
    return redirect('index')

def vervotos(request):
    context = {
        'votos': VotoPostagemEleitor.objects.filter(eleitor=request.user)
    }
    return render(request , 'seusvotos.html', context)

def alterar(request , id):

    postagem = Postagem.objects.get(id=id)
    voto = VotoPostagemEleitor.objects.filter(eleitor= request.user , postagem=postagem)
    voto.delete()

    messages.success(request , 'voto removido com sucesso ')

    return redirect('index')

def sobre( request , id ): 

    postagem = Postagem.objects.get(id=id)

    context = {'postagem': postagem}

    return render( request , 'sobre.html' , context)