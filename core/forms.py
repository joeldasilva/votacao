from django import forms
from .models import Postagem 


class PostagemModelForm( forms.ModelForm ):
    descricao = forms.CharField(
        widget = forms.Textarea(
        )
    )
    class Meta:
        model = Postagem
        fields = ('descricao' , 'imagem', 'autor')

