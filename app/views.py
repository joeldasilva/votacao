from django.shortcuts import render

from datetime import datetime
from django.contrib import messages 
# Create your views here.
from django.shortcuts import render
from django.contrib.auth.decorators import login_required

from datetime import datetime
from .models import dataVotacao



# Create your views here.
def login(request):
    da = dataVotacao.objects.all() 
    if da:
        for v in da:
            since = v.date_since
            final = v.date_final
            break
    else:
        since = '11/10/2022 00:00'
        final = '11/10/2022 00:00'
    
    def validate_date():
    
        since_data_e_hora = datetime.strptime(since , "%d/%m/%Y %H:%M")

        final_data_e_hora = datetime.strptime(final , "%d/%m/%Y %H:%M")

        if datetime.now() > since_data_e_hora and datetime.now() < final_data_e_hora:
            return 1
        elif datetime.now() < since_data_e_hora :
            return -1
        elif datetime.now() > final_data_e_hora :
            return 0 

    def  format_rest( param ):
        d = param / 86400
        rd = param % 86400

        h = rd/3600 
        rh = rd % 3600

        m = rh /60 
        rm = rh % 60 
        s = rm 

        return f'{ int(d) } d {int(h)} h {int(m)} m {int(s)} s'

    def get__diff():
        if validate_date() == 1:
            total = datetime.strptime( final , "%d/%m/%Y %H:%M") - datetime.now() 
            return f'{format_rest(total.total_seconds())} para o término das votações' 
        elif validate_date() == -1 :
            total = datetime.strptime( since , "%d/%m/%Y %H:%M") - datetime.now() 
            return f'{format_rest(total.total_seconds())} início das votações' 
        else: 
            return f'concluido'
    
    messages.warning(request , f'Para votar, clique no ícone localizado no canto inferior direito da foto' )
    
    ctx= {
        "validate": validate_date(),
        'rest' : get__diff()
    }
    return render(request, 'log.html' , ctx )

@login_required
def home(request):
    return render(request, 'home.html')



























    








