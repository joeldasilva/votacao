from django.db import models

# Create your models here.

class dataVotacao(models.Model):
    date_since = models.CharField("Data Inicio", max_length=17) 
    date_final = models.CharField("Data Final" , max_length=17)

class Meta:
    verbose_name = "Definir data de votacao"

    def __str__(self):
        return self.date_since