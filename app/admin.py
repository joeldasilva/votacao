from django.contrib import admin

# Register your models here.

from .models import dataVotacao 

class dataVotacaoAdmin( admin.ModelAdmin ): 
    fieldsets = [
        ('Data de Inicio', {'fields': ['date_since'], 'description':"dd/mm/yyyy hh:mm"}),
         ('Data de Fim ', {'fields': ['date_final'], 'description':"dd/mm/yyyy hh:mm"}),
        
    ]
    list_display = ("date_since" , "date_final" , )
    site_header = "Revelendo Fw"

admin.site.register(dataVotacao,  dataVotacaoAdmin)


