from django.contrib import admin
from django.urls import path, include
from django.contrib.auth import views as auth_views
from app import views
from .views import home
urlpatterns = [
    path('login/', views.login, name='login'),
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),
    path('social-auth/', include('social_django.urls', namespace='social')),
]